<ul class="mnu_info">
    <li><a href="#"><img src="images/ico_galeria.png" alt="galeria" class="left" /> <p class="txt_left">Galería</p></a></li>
    <li><a href="#"><img src="images/ico_instrucciones.png" alt="instrucciones" class="right" /> <p class="txt_right">Instrucciones</p></a></li>
    <li><a href="#"><img src="images/ico_perfil.png" alt="perfil" class="left" /> <p class="txt_left">Perfil</p></a></li>
    <li><a href="#"><img src="images/ico_manda.png" alt="manda tu opción" class="right" /> <p class="txt_right">Manda tu tostón</p></a></li>
</ul>

<?php
switch ($view) {
		case 4:
?>
			<script type="text/javascript">
				$('.mnu_info li').eq(0).addClass('active');
			</script>
<?php
			break;
		case 5:
?>				
			<script type="text/javascript">
				$('.mnu_info li').eq(1).addClass('active');
			</script>
<?php
			break;
		case 6:
?>
			<script type="text/javascript">
				$('.mnu_info li').eq(3).addClass('active');
			</script>
<?php
			break;
		case 7:
?>
			<script type="text/javascript">
				$('.mnu_info li').eq(2).addClass('active');
			</script>
<?php
			break;
	}
?>

<script type="text/javascript">
$('.mnu_info li').eq(2).on( "click", function(e) {
	e.preventDefault();
	$('.mnu_info li').removeClass('active');
  	$('.mnu_info li').eq(2).addClass('active');
  	changeContent(7);
});
$('.mnu_info li').eq(3).on( "click", function(e) {
	e.preventDefault();
	$('.mnu_info li').removeClass('active');
  	$('.mnu_info li').eq(3).addClass('active');
  	changeContent(6);
});
$('.mnu_info li').eq(1).on( "click", function(e) {
	e.preventDefault();
	$('.mnu_info li').removeClass('active');
  	$('.mnu_info li').eq(1).addClass('active');
  	changeContent(5);
});
$('.mnu_info li').eq(0).on( "click", function(e) {
	e.preventDefault();
	$('.mnu_info li').removeClass('active');
  	$('.mnu_info li').eq(0).addClass('active');
  	changeContent(4);
});
</script>