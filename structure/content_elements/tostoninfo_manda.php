<div class="col-xs-12 col-sm-12 col-md-12 mod_toston">
    <form id="formulario">
	<input name="FileInput" id="FileInput" type="file" />
</form>
	<p>Cuando termines el <strong>diseño "Frío y natural" de tu tostón,</strong> cárgalo en esta sección</p>
	<p>Diséñalo con tus propias manos o en el programa que quieras, pero no olvides documentarlo en nuestro Instagram <strong>@NesteaVzla</strong> utilizando el HT <strong>#PonleNESTEAAlTostón.</strong></p>
	<a  href="#" class="btn_cargar" title="Cargar tostón"></a>
</div>
<script type="text/javascript">
	$('.btn_cargar').click(function(e){
	  e.preventDefault();
	  $('#FileInput').click();
	});
</script>