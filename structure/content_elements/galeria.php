<!-- inicio MODAL TOSTON  -->
<div class="modal fade" id="ModalToston" tabindex="-1" role="dialog" >
  <div class="modal-dialog" role="document">
    <div class="modal-content modal_toston">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
            <div class="toston_boxfinal">
                <img src="images/pic_tostonfinal.png" alt="toston" />
                <p>...</p>
                <!--Redes Sociales-->
                <?php include_once('redes.php'); ?>
            </div>
      </div>
      <div class="modal-footer"></div>
    </div>
  </div>
</div>
<!-- fin MODAL TOSTON  -->
<div class="col-xs-12 col-sm-12 col-md-12 mod_toston">
    <p class="botones"><a href="#" class="btn btn_negro btn_left">Entra en acción</a> <a href="#" class="btn btn_negro btn_right">Crear mi propio tostón</a></p>
    <ul class="galeria">
        <?php    
           include_once('../inc/conexion.php');
           $result=$link->query('SELECT nombre AS nombre, apellido AS apellido, IFNULL(edad,0) AS edad, tostonUrl AS toston 
                                 FROM tb_toston AS t, tb_user AS user WHERE t.fbid = user.fbid AND t.autorizado > 0 LIMIT 3');          
           while($filas = $result->fetch_object()){
                echo '  <li onclick="showModal(this);"><a href=""  data-toggle="modal" data-target="#ModalToston"><img src='.$filas->toston.' alt="toston" />
                        <p>'.$filas->nombre.' | '.$filas->apellido.' | '.$filas->edad.' años</p></a></li>
                     ';
            } 
        ?>
    </ul>
    <a href="#" class="btn_cargarmas" title="cargar más"></a>
</div>
<script>
function showModal(p){
   var a = $(p).children('a').children('img').attr('src');
   var b = $(p).children('a').children('p').text();
   $('.toston_boxfinal img').attr('src',a);
   $('.toston_boxfinal p').html(b);
}
var offset=0;
$('.btn_cargarmas').click(function(){
  offset=offset+3;
  $.ajax({
    type: "POST",
    url: "./inc/more.php",
    dataType:"JSON",
    data:{offset:offset},
    success: function(data){
      console.log(data);
      if(data.resultado){
        $('.galeria').html(data.html);
      }else{
        offset = 0;
      }
    }
  });
});
</script>