<p>Selecciona el diseño que más te guste para tu <span class="naranja">TOSTÓN BOARD</span> ideal.</p>
<div class="carr_box">
    <div><a href="#"><img src="images/elementos/pic_toston01.png" alt="toston" /></a></div>
    <div><a href="#"><img src="images/elementos/pic_toston02.png" alt="toston" /></a></div>
    <div><a href="#"><img src="images/elementos/pic_toston03.png" alt="toston" /></a></div>
    <div><a href="#"><img src="images/elementos/pic_toston04.png" alt="toston" /></a></div>
    <div><a href="#"><img src="images/elementos/pic_toston05.png" alt="toston" /></a></div>
    <div><a href="#"><img src="images/elementos/pic_toston06.png" alt="toston" /></a></div>
    <div><a href="#"><img src="images/elementos/pic_toston07.png" alt="toston" /></a></div>
</div>

<div class="toston_box">
    <img id="showToston" src="images/elementos/pic_toston01.png" alt="toston" />
</div>
<a href="#" id="nextPd" class="btn_siguiente" title="siguiente"></a>
<script type="text/javascript">
    $('.carr_box').slick({
        infinite: true,
        speed: 600,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 320,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
        ]
    });
</script>