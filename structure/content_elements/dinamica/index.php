<div class="col-xs-12 col-sm-12 col-md-12 mod_toston">
    <!-- Menu -->
    <?php include_once('./structure/menu/nav_toston.php'); ?>
    <div class='pasouno'>
		<?php include_once('paso_uno.php'); ?>
	</div>
	<div class='pasodos_1'>
		<?php include_once('paso_dos.php'); ?>
	</div>
	<div class='pasodos_2'>
		<?php include_once('paso_dos_a.php'); ?>
	</div>
	<div class='pasodos_3'>
		<?php include_once('paso_dos_b.php'); ?>
	</div>
	<div class='pasotres'>
		<?php include_once('paso_tres.php'); ?>
	</div>
</div>
<script>
    $(document).ready(function(){
        $(".mnu_elem li a").click(function(e){
            e.preventDefault();	
            var parent = $(this).parent("li");
            if(parent.hasClass("active")){
                parent.removeClass("active");
            }else{
                parent.addClass("active");
            }
        });
        $("#nextPd").click(function(e){
            e.preventDefault();	
            $('.pasouno').hide();
            $('.pasodos_1').show();
            $('.mnu_toston li').eq(0).removeClass("active");
            $('.mnu_toston li').eq(1).addClass("active");
        });
        $("#nextA").click(function(e){
            e.preventDefault();	
            $('.pasodos_1').hide();
            $('.pasodos_2').show();
        });
        $("#nextB").click(function(e){
            e.preventDefault();	
            $('.pasodos_2').hide();
            $('.pasodos_3').show();
        });
        $("#nextPt").click(function(e){
            e.preventDefault();	
            $('.pasodos_3').hide();
            $('.pasotres').show();
            $('.mnu_toston li').eq(1).removeClass("active");
            $('.mnu_toston li').eq(2).addClass("active");
        });

        $("#ett0").click(function(e){
            e.preventDefault();	
            $('.pasouno, .pasodos_1, .pasodos_2, .pasodos_3, .pasotres').hide();
            $('.mnu_toston li').removeClass("active");
            $('.mnu_toston li').eq(0).addClass("active");
            $('.pasouno').show();
        });
        $("#ett1").click(function(e){
            e.preventDefault();	
            $('.pasouno, .pasodos_1, .pasodos_2, .pasodos_3, .pasotres').hide();
            $('.mnu_toston li').removeClass("active");
            $('.mnu_toston li').eq(1).addClass("active");
            $('.pasodos_1').show();
        });
        $("#ett2").click(function(e){
            e.preventDefault();	
            $('.pasouno, .pasodos_1, .pasodos_2, .pasodos_3, .pasotres').hide();
            $('.mnu_toston li').removeClass("active");
            $('.mnu_toston li').eq(2).addClass("active");
            $('.pasotres').show();
        });
        $('.btn_descarga').click(function(e){
        	e.preventDefault();
        	setUrls();
        });
        $('div.slick-slide').click(function(e){
        	e.preventDefault();
        	$("img").removeClass("active");
        	$(this).find("img").addClass("active");
        	var newSrc = $(this).find("img").attr('src');
        	var oldSrc = $('#showToston').attr('src');
			$('#showToston[src="' + oldSrc + '"]').attr('src', newSrc);
        });
        $('div.slick-slide').dblclick(function(e){
        	e.preventDefault();
        	$("img").removeClass("active");
        });
        $('.btn_cargar').on('click', function(e){
        	//uploadimg();
        });
        $('.btn_cargar').click(function(e){
          e.preventDefault();
          $('#FileInput').click();
        });
		
    });
</script>