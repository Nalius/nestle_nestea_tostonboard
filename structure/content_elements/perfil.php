<div class="col-xs-12 col-sm-12 col-md-12 mod_toston">
    <div class="mod_perfil">
        <img src="images/loader.gif" alt="foto perfil"  class="avatar" />
        <p>...</p>
    </div>
    <div class="mod_perfiltxt">
        <p>Ten paciencia, nuestro staff ha recibido muchos tostones increíbles.<br>Pronto te responderemos</p>
        <div class="aceptado"><h5>Aprobado</h5><p>¡Revisa tu Email! </p></div>
        <div class="recibido"><h5>Recibido</h5><p>¡Revisa tu Email! </p></div>
        <div class="rechazado"><h5>Rechazado</h5><p>¡Revisa tu Email! </p></div>
    </div>
    <div class="toston_boxfinal" >
        <img src="images/pic_tostonfinal.png" alt="toston" />
        <p class="profile_data">...</p>
        <!--Redes Sociales-->
        <?php include_once('redes.php'); ?>
    </div>
</div>
<script type="text/javascript">
         FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                FB.api('/me',{fields: 'email,first_name,last_name,gender'}, function (resp) {
                    if(resp && !resp.error){
                        $('.mod_perfil p').html(resp.first_name+" | "+resp.last_name+" | "+"23 Años");
                        $('.mod_perfil img').attr('src','https://graph.facebook.com/'+resp.id+'/picture?type=large');
                        $('.profile_data').html(resp.first_name+" | "+resp.last_name+" | "+"23 Años");
                        $.ajax({
                            type: "POST",
                            url: "./inc/perfiltoston.php",
                            data:{id:resp.id},
                            dataType: "JSON",
                            success: function(data){
                                console.log(data);
                                if(data.resultado.resultado){
                                    $('.toston_boxfinal img').attr('src',data.toston);
                                }
                            }
                        });
                    }
                });        
            }else{
                console.log('No Conectado');                
            }
        });            
</script>