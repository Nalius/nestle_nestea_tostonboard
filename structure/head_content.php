<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>NESTLÉ® TostonBoard </title>
<!-- Jquery -->
<script src="js/lib/jquery-1.11.3.min.js"></script>
<script src="js/lib/jquery-migrate-1.2.1.min.js"></script>
<!-- https://github.com/eligrey/FileSaver.js/ -->
<script src="js/lib/FileSaver.min.js"></script>
<!-- https://stuk.github.io/jszip/ -->
<script src="js/lib/jszip.min.js"></script>
<!-- http://kenwheeler.github.io/slick/ -->
<script src="js/lib/slick.min.js"></script>

<!-- Bootstrap -->
<script src="js/lib/bootstrap.min.js"></script>
<!-- Style Sheet -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/styles_toston.css" rel="stylesheet">
<link href="css/styles_tostonmob.css" rel="stylesheet">
<link href="css/slick-1.5.7/slick.css" rel="stylesheet">
