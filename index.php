<?php
	if(!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == ""){
	    $redirect = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	    header("Location: $redirect");
	}
?>
<!DOCTYPE html>
<html lang="en">

	<head>
		<?php include_once('structure/head_content.php'); ?>
	</head>  
	<body>

		<div class="wrapper">
			<div class="velito"><img src="images/loader.gif"></div>
	        <div class="container-fluid">

		        <?php include_once('structure/content.php'); ?>
		        <?php //include_once('structure/content_elements/pre_instrucciones.php') ?>
	        </div>
	    </div> 
	</body>
	<script type="text/javascript">

	</script>
</html>