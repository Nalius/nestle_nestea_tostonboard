<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>NESTLÉ® TostonBoard </title>
    <!-- Bootstrap -->
    <link href="<?= asset_url(); ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= asset_url(); ?>css/styles_toston.css" rel="stylesheet">
    <link href="<?= asset_url(); ?>css/styles_tostonmob.css" rel="stylesheet">
  </head>
  <body>
      <div class="wrapper">
        <div class="container-fluid">
          <div class="row"> 
            <?php 
              //load content
              $this->load->view($content); 
            ?>
          </div>
          <!-- inicio FOOTER -->
          <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 footer">
                  <div class="copy">
                      <p><img src="<?= asset_url(); ?>images/pic_logonestlefooter.png" alt="nestle" /> &reg;MARCAS REGISTRADAS USADAS BAJO LICENCIA DE SU TITULAR, SOCIÉTÉ DES PRODUITS NESTLÉ S.A., CASE POSTALE 353, 1800 VEVEY, SUIZA. 2014 NESTLÉ VENEZUELA,S.A. RIF: J-00012926-6 <br>
                      <a href="https://ww1.nestle.com.ve/savoy/branding2014/politicas.html" target="_blank">POLÍTICAS DE PRIVACIDAD</a> | <a href="http://www.nestle.com.ve/info/tc" target="_blank">TÉRMINOS Y CONDICIONES</a></p>
                  </div>   
              </div>
          </div>
          <!-- fin FOOTER -->
        </div> 
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?= asset_url(); ?>js/bootstrap.min.js"></script>
  </body>
</html>