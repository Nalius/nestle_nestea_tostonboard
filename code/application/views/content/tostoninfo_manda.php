<div class="container-fluid">
    <!-- inicio INFO HOME -->
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 tope">
            <img src="<?= asset_url(); ?>images/pic_logotema.png" alt="Ponle Nestlé® a ese tostón" class="logotema"  />
            <img src="<?= asset_url(); ?>images/bckg_tope.jpg" alt="fondo" class="fondo" />
            <img src="<?= asset_url(); ?>images/bckg_topemob.jpg" alt="fondo" class="fondomob" />
            <!-- Menu -->
            <?= $menu_info; ?>
        </div>
    </div>   
    <!-- fin HEADER -->
    
    <!-- inicio INFO HOME -->
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 mod_toston">
            <p><strong>Manda tu tostón</strong></p>
            <p>Cuando termines el <strong>diseño "Frío y natural" de tu tostón,</strong> cárgalo en esta sección</p>
            <p>Diséñalo con tus propias manos o en el programa que quieras, pero no olvides documentarlo en nuestro Instagram <strong>@NesteaVzla</strong> utilizando el HT <strong>#PonleNESTEAAlTostón.</strong></p>
            <a href="" class="btn_cargar" title="Cargar tostón"></a>
        </div>
    </div>
    <!-- fin INFO HOME -->
</div> 