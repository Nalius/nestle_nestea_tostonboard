<div class="container-fluid">
    <!-- inicio INFO HOME -->
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 tope">
            <img src="<?= asset_url(); ?>images/pic_logotema.png" alt="Ponle Nestlé® a ese tostón" class="logotema"  />
            <img src="<?= asset_url(); ?>images/bckg_tope.jpg" alt="fondo" class="fondo" />
            <img src="<?= asset_url(); ?>images/bckg_topemob.jpg" alt="fondo" class="fondomob" />
            <!-- Menu -->
            <?= $menu_info; ?>
        </div>
        </div>
    </div>   
    <!-- fin HEADER -->
    <!-- inicio INFO HOME -->
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 mod_toston">
            <img src="<?= asset_url(); ?>images/pic_instrucciones.png" alt="instrucciones" class="titinstrucc" />
            <p class="txt_instr"><strong>¿Conoces a los panas de TOSTÓN BOARD?</strong>
            Si los conoces, sigue leyendo y si no pues hacen unos BALANCE BOARD increíbles…</p>
            <p class="txt_instr"><strong>¿Te quieres ganar uno?</strong>
            Selecciona el diseño de tu TOSTóN BOARD y guárdala en tu PC. Déjate inspirar y comienza a trabajar en un diseño brutal, "Frío y Natural" para tu TOSTÓN BOARD ideal.</p>
            <p class="txt_instr"><strong>&iexcl;Métele elementos  NESTEA<span>&reg;</span>!</strong> Para ello, te dejamos en el TAB todo lo que necesitas.</p> 
            <p class="txt_instr"><strong>Documenta tu proceso creativo,</strong> sube fotos en Instagram <strong>usando el hashtag #PonleNesteaAlTostón</strong> y taguea a todos los panas que puedas para que te den muchos “Likes”.</p>
            <p class="txt_instr"><strong>Tendrás hasta el xx/xx/xxxx</strong> para <strong>subir tu diseño en nuestro Facebook,</strong> así que actívate.</p>
            <p class="txt_instr">&iexcl;Deja volar tu imaginación! Porque si ganas, <strong>el Tostón Board de regalo, tendrá tu diseño.</strong></p>
            <p class="txt_instr"><strong>Y además te llevarás una tabla WACOM</strong> para que sigas diseñando…</p>
            
        </div>
    </div>
    <!-- fin INFO HOME -->
</div> 