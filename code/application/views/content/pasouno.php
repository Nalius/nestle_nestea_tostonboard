<div class="col-xs-12 col-sm-12 col-md-12 mod_toston">
    <!-- Menu -->
    <?= $menu_nav; ?>
    <p>Selecciona el diseño que más te guste para tu <span class="naranja">TOSTÓN BOARD</span> ideal.</p>
    <div class="carr_toston">
        <a href="" class="fl_ant"></a>
        <a href="" class="fl_sig"></a>
        <div class="carr_box">
            <div><a href=""><img src="<?= asset_url(); ?>images/pic_toston01.png" alt="toston" /></a></div>
            <div class="active"><a href=""><img src="<?= asset_url(); ?>images/pic_toston02.png" alt="toston" /></a></div>
            <div><a href=""><img src="<?= asset_url(); ?>images/pic_toston03.png" alt="toston" /></a></div>
            <div><a href=""><img src="<?= asset_url(); ?>images/pic_toston04.png" alt="toston" /></a></div>
            <div><a href=""><img src="<?= asset_url(); ?>images/pic_toston05.png" alt="toston" /></a></div>
        </div>
    </div>
    <div class="toston_box">
        <img src="<?= asset_url(); ?>images/pic_toston02.png" alt="toston" />
    </div>
    <a href="<?= $next_url;?>" class="btn_siguiente" title="siguiente"></a>
</div>