<!-- inicio MODAL TOSTON  -->
<div class="modal fade" id="ModalToston" tabindex="-1" role="dialog" >
  <div class="modal-dialog" role="document">
    <div class="modal-content modal_toston">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
            <div class="toston_boxfinal">
                <img src="<?= asset_url(); ?>images/pic_tostonfinal.png" alt="toston" />
                <p>David Jurin | Hombre | 21 años</p>
                <!--Redes Sociales-->
                <?= $redes; ?>
            </div>
      </div>
      <div class="modal-footer"></div>
    </div>
  </div>
</div>
<!-- fin MODAL TOSTON  -->
<div class="container-fluid">
    <!-- inicio INFO HOME -->
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 tope">
            <img src="<?= asset_url(); ?>images/pic_logotema.png" alt="Ponle Nestlé® a ese tostón" class="logotema"  />
            <img src="<?= asset_url(); ?>images/bckg_tope.jpg" alt="fondo" class="fondo" />
            <img src="<?= asset_url(); ?>images/bckg_topemob.jpg" alt="fondo" class="fondomob" />
            <!-- Menu -->
            <?= $menu_info; ?>
        </div>
    </div>   
    <!-- fin HEADER -->
    <!-- inicio INFO HOME -->
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 mod_toston">
            <p class="botones"><a href="" class="btn btn_negro btn_left">Entra en acción</a> <a href="" class="btn btn_negro btn_right">Crear mi propio tostón</a></p>
            <ul class="galeria">
                <li><a href=""  data-toggle="modal" data-target="#ModalToston"><img src="<?= asset_url(); ?>images/pic_tostonfinal.png" alt="toston" />
                    <p>David Jurin | Hombre | 21 años</p></a></li>
                <li><a href=""><img src="<?= asset_url(); ?>images/pic_tostonfinal.png" alt="toston" />
                    <p>David Jurin | Hombre | 21 años</p></a></li>
                <li><a href=""><img src="<?= asset_url(); ?>images/pic_tostonfinal.png" alt="toston" />
                    <p>David Jurin | Hombre | 21 años</p></a></li>
                <li><a href=""><img src="<?= asset_url(); ?>images/pic_tostonfinal.png" alt="toston" />
                    <p>David Jurin | Hombre | 21 años</p></a></li>
                <li><a href=""><img src="<?= asset_url(); ?>images/pic_tostonfinal.png" alt="toston" />
                    <p>David Jurin | Hombre | 21 años</p></a></li>
                <li><a href=""><img src="<?= asset_url(); ?>images/pic_tostonfinal.png" alt="toston" />
                    <p>David Jurin | Hombre | 21 años</p></a></li>
                <li><a href=""><img src="<?= asset_url(); ?>images/pic_tostonfinal.png" alt="toston" />
                    <p>David Jurin | Hombre | 21 años</p></a></li>
                <li><a href=""><img src="<?= asset_url(); ?>images/pic_tostonfinal.png" alt="toston" />
                    <p>David Jurin | Hombre | 21 años</p></a></li>
                <li><a href=""><img src="<?= asset_url(); ?>images/pic_tostonfinal.png" alt="toston" />
                    <p>David Jurin | Hombre | 21 años</p></a></li>
            </ul>
            <a href="#" class="btn_cargarmas" title="cargar más"></a>
        </div>
    </div>
    <!-- fin INFO HOME -->
</div> 