<div class="col-xs-12 col-sm-12 col-md-12 mod_toston">
    <!-- Menu -->
    <?= $menu_nav; ?>
    <p>Selecciona todos los elementos que quieras para completar tu diseño. <br>No olvides incluir a <strong>NESTEA&reg;</strong> en tu tostón.</p>
    <ul class="mnu_elem">
        <li class="active"><a href=""><img src="<?= asset_url(); ?>images/elementos/pic_elem01.jpg" alt="elemento" /></a></li>
        <li><a href=""><img src="<?= asset_url(); ?>images/elementos/pic_elem02.jpg" alt="elemento" /></a></li>
        <li><a href=""><img src="<?= asset_url(); ?>images/elementos/pic_elem03.jpg" alt="elemento" /></a></li>
        <li class="active"><a href=""><img src="<?= asset_url(); ?>images/elementos/pic_elem04.jpg" alt="elemento" /></a></li>
        <li class="active"><a href=""><img src="<?= asset_url(); ?>images/elementos/pic_elem05.jpg" alt="elemento" /></a></li>
        <li><a href=""><img src="<?= asset_url(); ?>images/elementos/pic_elem06.jpg" alt="elemento" /></a></li>
        <li><a href=""><img src="<?= asset_url(); ?>images/elementos/pic_elem07.jpg" alt="elemento" /></a></li>
        <li><a href=""><img src="<?= asset_url(); ?>images/elementos/pic_elem08.jpg" alt="elemento" /></a></li>
        <li><a href=""><img src="<?= asset_url(); ?>images/elementos/pic_elem09.jpg" alt="elemento" /></a></li>
        <li><a href=""><img src="<?= asset_url(); ?>images/elementos/pic_elem10.jpg" alt="elemento" /></a></li>
        <li><a href=""><img src="<?= asset_url(); ?>images/elementos/pic_elem11.jpg" alt="elemento" /></a></li>
        <li><a href=""><img src="<?= asset_url(); ?>images/elementos/pic_elem12.jpg" alt="elemento" /></a></li>
        <li><a href=""><img src="<?= asset_url(); ?>images/elementos/pic_elem13.jpg" alt="elemento" /></a></li>
        <li><a href=""><img src="<?= asset_url(); ?>images/elementos/pic_elem14.jpg" alt="elemento" /></a></li>
        <li><a href=""><img src="<?= asset_url(); ?>images/elementos/pic_elem15.jpg" alt="elemento" /></a></li>
        <li><a href=""><img src="<?= asset_url(); ?>images/elementos/pic_elem16.jpg" alt="elemento" /></a></li>
    </ul>
    <a href="<?= $next_url;?>" class="btn_siguiente" title="siguiente"></a>
</div>