<?php
if(!defined ('BASEPATH')) exit ('No direct script access allowed');
	class Viewcontents{
		private $arr_menu;
		
		public function __construct($arr){
			$this->arr_menu_nav = $arr[0];
			$this->arr_menu_info = $arr[1];
			$this->arr_redes = $arr[2];
		}

		public function construirRedes(){
			$ret_redes = '<ul class="redes">';
			if( !empty($this->arr_redes) ) {
				foreach ($this->arr_redes as $posicion => $url){
					switch ($posicion) {
						case '0':
								$ret_redes .= '<li class="btn_fb"><a href="'.$url.'" title="facebook"></a></li>';
								break;
							case '1':
								$ret_redes .= '<li class="btn_tw"><a href="'.$url.'" title="twitter"></a></li>';
								break;
							case '2':
								$ret_redes .= '<li class="btn_ig"><a href="'.$url.'" title="instagran"></a></li>';
								break;
					}
				}
			}
			$ret_redes .= '</ul>';
			return $ret_redes;
		}

		public function construirMenu($type){
			if($type == 'menu_nav'){
				$ret_menu = '<ul class="mnu_toston">';
				if( !empty($this->arr_menu_nav) ) {
					foreach ($this->arr_menu_nav as $posicion => $opcion){
						$ret_menu .= "<li><a href=''>".$opcion."</a></li>";
					}
				}
				$ret_menu .= '</ul>';
			}else{
				$url = asset_url().'images/ico_galeria.png';

				$ret_menu = '<ul class="mnu_info">';
				if( !empty($this->arr_menu_info) ) {
					foreach ($this->arr_menu_info as $posicion => $opcion){
						if (($posicion+1)%2==0){
						    $result="class='right' /> <p class='txt_right'>";
						}else{
						    $result="class='left' /> <p class='txt_left'>";
						}
						switch ($posicion) {
							case '0':
								$url = asset_url().'images/ico_galeria.png';
								$ret_menu .= "<li><a href=''><img src='".$url."' alt='".$opcion."' ".$result.$opcion."</p></a></li>";
								break;
							case '1':
								$url = asset_url().'images/ico_instrucciones.png';
								$ret_menu .= "<li><a href=''><img src='".$url."' alt='".$opcion."' ".$result.$opcion."</p></a></li>";
								break;
							case '2':
								$url = asset_url().'images/ico_perfil.png';
								$ret_menu .= "<li><a href=''><img src='".$url."' alt='".$opcion."' ".$result.$opcion."</p></a></li>";
								break;
							case '3':
								$url = asset_url().'images/ico_manda.png';
								$ret_menu .= "<li><a href=''><img src='".$url."' alt='".$opcion."' ".$result.$opcion."</p></a></li>";
								break;
						}
					}
				}
				$ret_menu .= '</ul>';
			}
			return $ret_menu;
		}
	}
?>