<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Instrucciones extends CI_Controller {

	function  __construct(){
		parent::__construct();
		$menu_mav = array('Escoje tu tostón', 'Enchula tu tostón', 'Manda el tostón');
		$menu_info = array('Galería', 'Instrucciones', 'Perfil', 'Manda tu tostón');
		$redes_url =  array('facebook', 'twitter', 'instagram');
		$multi_array = array($menu_mav, $menu_info, $redes_url);

		$this->load->library('viewcontents', $multi_array);

		$this->data = array( 
			'redes' => $this->viewcontents->construirRedes(),
			'menu_nav' => $this->viewcontents->construirMenu('menu_nav'),
			'menu_info' => $this->viewcontents->construirMenu('menu_info')
		);
	}

	public function index(){
		$data = $this->data;
		$data['content'] = 'content/instrucciones';
		$data['next_url'] = site_url('pasouno	');
		$this->load->view('template', $data);
	}
}
?>